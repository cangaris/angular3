import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ArticleService } from "../services/article.service";
import { Article } from "../models/Article";
import { ActivatedRoute, Router } from "@angular/router";
import { HttpErrorResponse } from "@angular/common/http";

@Component({
  selector: 'app-article-form',
  templateUrl: './article-form.component.html',
  styleUrls: ['./article-form.component.css']
})
export class ArticleFormComponent implements OnInit {

  form!: FormGroup;

  constructor(private fb: FormBuilder, private service: ArticleService,
              private router: Router, private activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot.queryParamMap.get('id');
    if (id) {
      const parsed = parseInt(id);
      if (!isNaN(parsed)) {
        this.service.getArticle(parsed).subscribe(article => {
          this.form.patchValue(article);
        });
      }
    }
    this.form = this.fb.group({
      id: null,
      title: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(20)]],
      content: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(200)]],
      category: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(20)]],
    });
  }

  onSubmit(): void {
    if (this.form.valid) {
      const data: Article = this.form.value;
      if (data.id) {
        this.edit(data);
      } else {
        this.add(data);
      }
    } else {
      alert("Formularz niepoprawny");
    }
  }

  private add(data: Article) {
    this.service.addArticle(data).subscribe(
      () => this.router.navigate(['/articles']),
      error => {
        console.error(error);
        alert("Nieudało się dodać artykułu");
      }
    )
  }

  private edit(data: Article) {
    this.service.editArticle(data).subscribe(
      () => this.router.navigate(['/articles']),
      (error: HttpErrorResponse) => {
        const errors: string[] = error.error.message;
        alert("Nieudało się edytować artykułu: " + errors.join(', '));
      }
    )
  }
}
