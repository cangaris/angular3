export enum Role {
  ADMIN = 'ADMIN',
  MANAGER = 'MANAGER',
  AUTHOR = 'AUTHOR'
}

export interface User {
  id: number;
  username: string;
  role: Role;
  phone: UserPhone;
  emails: UserEmail[];
  addresses: UserAddress[];
}

export interface UserPhone {
  id: number;
  phone: string;
}

export interface UserEmail {
  id: number;
  email: string;
}

export interface UserAddress {
  id: number;
  city: string;
  street: string;
}
