export enum ArticleCategory {
  TECHNICAL = 'TECHNICAL',
  GUIDE = 'GUIDE',
  OTHER = 'OTHER'
}

export interface Article {
  id?: number;
  title: string;
  content: string;
  category: ArticleCategory;
}
