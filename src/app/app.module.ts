import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { UserComponent } from './user/user.component';

import localePl from '@angular/common/locales/pl';
import localeExtraPl from '@angular/common/locales/extra/pl';
import { registerLocaleData } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { RouterModule, Routes } from "@angular/router";
import { ArticleComponent } from './article/article.component';
import { UsersComponent } from './users/users.component';
import { ArticlesComponent } from './articles/articles.component';
import { LoginComponent } from './login/login.component';
import { ReactiveFormsModule } from "@angular/forms";
import { ArticleFormComponent } from './article-form/article-form.component';

registerLocaleData(localePl, localeExtraPl);

const routes: Routes = [
  {path: 'users', component: UsersComponent},
  {path: 'articles', component: ArticlesComponent},
  {path: 'article-form', component: ArticleFormComponent},
  {path: 'login', component: LoginComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    UsersComponent,
    ArticleComponent,
    ArticlesComponent,
    LoginComponent,
    ArticleFormComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    ReactiveFormsModule,
  ],
  providers: [{ provide: LOCALE_ID, useValue: "pl" }],
  bootstrap: [AppComponent]
})
export class AppModule { }
