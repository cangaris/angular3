import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Article } from "../models/Article";
import { ArticleService } from "../services/article.service";

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent {

  @Input() articleData!: Article;
  @Output() removed = new EventEmitter<number>();

  constructor(private articleService: ArticleService) { }

  removeArticle(id: number) {
    this.articleService.deleteArticle(id).subscribe(() => {
      this.removed.emit(id);
    });
  }
}
