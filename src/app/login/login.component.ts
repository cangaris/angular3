import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from "@angular/forms";
import { UserService } from "../services/user.service";
import { Router } from "@angular/router";

interface LoginForm {
  username: string;
  password: string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form!: FormGroup;

  constructor(private fb: FormBuilder, private service: UserService, private router: Router) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      username: 'admin',
      password: 'TrudneHasłoAdmina',
    });
  }

  onSubmit() {
    const loginData: LoginForm = this.form.value;
    this.service.login(loginData.username, loginData.password).subscribe(
      () => {
        this.service.userLogged.emit(true);
        this.router.navigate(['/users']);
      },
      () => {
        this.service.userLogged.emit(false);
        alert("Niepoprawne dane logowania");
      },
    );
  }
}
