import { Component, OnInit } from '@angular/core';
import { User } from "../models/User";
import { UserService } from "../services/user.service";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  data: User[] = [];
  constructor(private service: UserService) {}

  ngOnInit(): void {
    this.service.getUsers().subscribe(users => { this.data = users; });
  }
}
