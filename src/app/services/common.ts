export class Common {

  static getOptions(responseType: string = 'json'): object {
    return {
      responseType: responseType,
      withCredentials: true,
    };
  }
}
