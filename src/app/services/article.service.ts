import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { Observable } from "rxjs";
import { Article } from "../models/Article";
import { Common } from "./common";

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  private readonly api = environment.apiUri;

  constructor(private client: HttpClient) { }

  getArticles(): Observable<Article[]> {
    return this.client.get<Article[]>(`${this.api}/rest/articles`, Common.getOptions());
  }

  getArticle(id: number): Observable<Article> {
    return this.client.get<Article>(`${this.api}/rest/articles/${id}`, Common.getOptions());
  }

  addArticle(article: Article): Observable<Article> {
    return this.client.post<Article>(`${this.api}/rest/articles`, article, Common.getOptions())
  }

  editArticle(article: Article): Observable<Article> {
    return this.client.put<Article>(`${this.api}/rest/articles/${article.id}`, article, Common.getOptions())
  }

  deleteArticle(id: number): Observable<void> {
    return this.client.delete<void>(`${this.api}/rest/articles/${id}`, Common.getOptions());
  }
}
