import { EventEmitter, Injectable } from '@angular/core';
import { User } from "../models/User";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment } from "../../environments/environment";
import { Common } from "./common";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private readonly api = environment.apiUri;
  readonly userLogged = new EventEmitter<boolean>();

  constructor(private client: HttpClient) {
  }

  login(username: string, password: string): Observable<unknown> {
    const data = new FormData();
    data.set('username', username);
    data.set('password', password);
    return this.client.post(`${this.api}/login`, data, Common.getOptions('text'));
  }

  logout(): Observable<unknown> {
    return this.client.get(`${this.api}/logout`, Common.getOptions('text'));
  }

  getUsers(): Observable<User[]> {
    return this.client.get<User[]>( `${this.api}/rest/users`, Common.getOptions());
  }
}
