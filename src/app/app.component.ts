import { Component, OnInit } from '@angular/core';
import { UserService } from "./services/user.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  isLogged = false;

  constructor(private service: UserService, private router: Router) {
  }

  ngOnInit(): void {
    this.service.userLogged.subscribe(logged => this.isLogged = logged);
  }

  logout() {
    this.service.logout().subscribe();
    this.service.userLogged.emit(false);
    this.router.navigate(['/login']);
  }
}
