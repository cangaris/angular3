import { Component, OnInit } from '@angular/core';
import { Article } from "../models/Article";
import { ArticleService } from "../services/article.service";
import { UserService } from "../services/user.service";

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.css']
})
export class ArticlesComponent implements OnInit {

  data: Article[] = [];

  constructor(private service: ArticleService) {}

  ngOnInit(): void {
    this.service.getArticles().subscribe(articles => {
      this.data = articles;
    });
  }

  removeArticle(id: number) {
    const foundIdx = this.data.findIndex(value => value.id === id);
    this.data.splice(foundIdx, 1);
  }
}
